<?php

$txt = array();

$txt['health'] = "Health";
$txt['health_emoji'] = "🏥";
$txt['happyness'] = "Happyness";
$txt['happyness_emoji'] = "😄";
$txt['hunger'] = "Hunger";
$txt['hunger_emoji'] = "🥢";
$txt['thirst'] = "Thirst";
$txt['thirst_emoji'] = "💧";
$txt['money'] = "Money";
$txt['money_emoji'] = "💰";
$txt['debt'] = "Debt";
$txt['debt_emoji'] = "💲";


$txt = [ 


    'health_potion_weak' => [
        'name' => "Health potion, Weak",
        'description' => "Restore 10 HP"
    ],
    'health_potion_strong' => [
        'name' => "Health potion, Strong",
        'description' => "Restore 30 HP"
    ],
    'health_potion_restore' => [
        'name' => "Restore health potion",
        'description' => "Restore Full HP"
    ],

    'food_pasta' => [
        'name' => "Pasta",
        'description' => "A plate of pasta. Restore {$txt['hunger']} by 20 GP"
    ],

    'user_profile' => "
{{!}}

<b>{$txt['health_emoji']}{$txt['health']}</b> - {{!}}
{{!}}
<b>{$txt['happyness_emoji']}{$txt['happyness']}</b> - {{!}}
{{!}}
<b>{$txt['hunger_emoji']}{$txt['hunger']}</b> - {{!}}
{{!}}
<b>{$txt['thirst_emoji']}{$txt['thirst']}</b> - {{!}}
{{!}}
<b>{$txt['money_emoji']}{$txt['money']}</b> - {{!}}
{{!}}
<b>{$txt['debt_emoji']}{$txt['debt']}</b> - {{!}}
{{!}}
    "
        
    

];





?>
