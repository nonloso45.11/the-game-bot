<?php

interface Objectss{

    public function getName();

    public function getDescription();

    public function getPrice();

    public function getImage();

    public function getEmoji();

    public function isDrinkable();

    public function isEatable();

}

class Objects implements Objectss{
    protected string $name = "";
    protected string $description = "";
    protected float $price = 0;
    protected string $image = "";
    protected string $emoji = "";
    protected bool $drinkable = false;
    protected bool $eatable = false;

    public function getName(){
        return $this->name;
    }

    public function getDescription(){
        return $this->description;
    }

    public function getPrice(){
        return $this->price;
    }

    public function getImage(){
        return $this->image;
    }

    public function getEmoji(){
        return $this->emoji;
    }

    public function isDrinkable(){
        return $this->drinkable;
    }

    public function isEatable(){
        return $this->eatable;
    }
    
}


class HealthPotionWeak extends Objects{

    public function __construct(){
        $this->name = getTxt("health_potion_weak")['name'];
        $this->description = getTxt("health_potion_weak")['description'];
        $this->price = 10;
        $this->emoji = "🧪";
        $this->drinkable = true;

    }
}

class HealthPotionStrong extends Objects{

    public function __construct(){
        $this->name = getTxt("health_potion_strong")['name'];
        $this->description = getTxt("health_potion_strong")['description'];
        $this->price = 20.5;
        $this->emoji = "🧪";

    }
}

class HealthPotionRestore extends Objects{

    public function __construct(){
        $this->name = getTxt("health_potion_restore")['name'];
        $this->description = getTxt("health_potion_restore")['description'];
        $this->price = 50;
        $this->emoji = "🧪";
    }
}

class FoodPasta extends Objects{

    public function __construct(){
        $this->name = getTxt("food_pasta")['name'];
        $this->description = getTxt("food_pasta")['description'];
        $this->price = 5;
        $this->emoji = "🍝";
        $this->eatable = true;
    }

}

?>
