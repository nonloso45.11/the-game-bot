<?php

const TOKEN = "<BOT_TOKEN>";

const ADMIN = <YOUR_USER_ID>;

const INTEREST_RATE = 0.12; //%  between 0 and 1

const NL = "\n";

try {
    $connessione = new PDO('mysql:dbname=<YOUR_DATABASE_NAME>;host=<YOUR_HOST>;charset=utf8mb4', '<YOUR_USER>', '<YOUR_PASSWORD>');
    $connessione->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e){
    http_response_code(500);
    echo $e->getMessage();
    exit;
}





?>
