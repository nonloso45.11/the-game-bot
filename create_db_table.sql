
CREATE TABLE `user` (
  `user_id` bigint NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `language` varchar(5) DEFAULT NULL,
  `health` tinyint NOT NULL DEFAULT '100',
  `happyness` tinyint NOT NULL DEFAULT '100',
  `hunger` tinyint NOT NULL DEFAULT '0',
  `thirst` tinyint NOT NULL DEFAULT '0',
  `money` float NOT NULL DEFAULT '100',
  `debt` float NOT NULL DEFAULT '0',
  `last_seen_online` int DEFAULT NULL,
  `last_story` smallint NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);
COMMIT;
