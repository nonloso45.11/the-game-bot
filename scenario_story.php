<?php




$bot->method = "sendMessage";
$bot->params['chat_id'] = $user->user_id;


$story = json_decode(file_get_contents("story1.json"), true);




/*
Eveyone can add new stories in the format
Some text here bla bla bla
-Reply 1-> 2
[HP -1]
[MONEY +100] 
-Reply 2 -> ?
-Reply 3 -> 3
[SOMETHING ]
*/


function prepareNextText($what, &$bot){
    global $story;
    $s = sizeof($story[$what]['send']);
    foreach($story[$what]['send'] as $v){
        $s--;
        if(key($v) == "wait"){
            sleep($v[key($v)]);
        }
        else if(key($v) == "text"){
            $bot->method = "sendMessage";
            $bot->params['text'] = $v[key($v)];
            if($s > 0){
                $bot->callApi();
            }
        }
        else if(key($v) == "photo"){
            $bot->method = "sendPhoto";
            $bot->params['photo'] = $v[key($v)];
            if($s > 0){
                $bot->callApi();
            }
        }
    }
    
    $keyboard = array();
    foreach($story[$what]['answers'] as $v){
        array_push($keyboard, [['text' => $v['text']]]);
    }

    $keyboard = ['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => true, 'input_field_placeholder' => "Choose.."];
    $bot->params['reply_markup'] = json_encode($keyboard, true);
    
}



if($update['message']['text'] == "/start"){
    $user->last_story = 0;
}
else{  
    $replyTo = $story[$user->last_story];
    foreach($story[$user->last_story]['answers'] as $v){
        if($update['message']['text'] == $v['text']){
            if(isset($v['effect'])){
                //do action
            }
            $user->last_story = $v['call'];
        }
    }
}
try{
    $query = $connessione->prepare("UPDATE user SET last_story = :lst WHERE user_id = :chid");
    $query->bindParam(':chid', $user->user_id);
    $query->bindParam(':lst', $user->last_story);

    $query->execute();
}
catch(PDOException $e){
    $bot->params['text'] = $e->getMessage();
    $bot->callApi();
    
}

prepareNextText($user->last_story, $bot);

$bot->callApi();







exit;




?>
