<?php

class Bot{

    private $token;
    public $method;
    public $params;


    public function __construct($token){
        $this->token = $token;
    }

    
    public function callApi($method = null, $params = null, $checkError = true){
        if(!isset($method)){   $method = $this->method;  }
        if(!isset($params)){   $params = $this->params;  }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot" . $this->token . "/$method");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
    
        if($checkError){
            $this->checkErrorApiCall($result);
        }
    
        return $result;
    }

    private function checkErrorApiCall($result){
        $r = json_decode($result, true);
    
        if($r['ok']){       return;     }
    
        $this->callApi("sendMessage", [
            'chat_id' => ADMIN,
            'text' => "#ERRORE: $result"
        ], false);
    
    }
}


require_once __DIR__ . "/locale/en.php"; 

function getTxt(string $text, array $replacements = array(), string $language = ""){
    global $txt;
    if($language == ""){
        // global $user;
        // if(isset($user)){
        //     $language = $user->getLanguage;
        // }
        // else{
        //     $language = "en";
        // }
        $language = "en";
        
    }

    
    // if($language == "en"){  require_once __DIR__ . "/locale/en.php";    }

    $translation = $txt[$text];

    foreach($replacements as $r){
        $translation = preg_replace("/{{!}}/", $r, $translation, 1);
    }


    return $translation;

}








?>
