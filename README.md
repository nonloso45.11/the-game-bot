# The game bot

This is a proof of concept for a telegram game bot. You can try the story scenario at [@gioko_bot](https://t.me/gioko_bot)

If you are interested in joining the project contact [@non_lo_so_piu](https://t.me/non_lo_so_piu)

Updates on the project at [t.me/agamebotdev](https://t.me/agamebotdev)

## Game ideas
- Life simulator, more text based
- Multiple answers story game, with more freedom
- Else

## Goals of the project
- Learn, experiment, improve coding/deployment skills
- Make the code as professional as possible
- Learn more about open source and its practices
- Create something cool and fun

## Influences for the game
- Pokemon
- Generic build your empire game
- Generic role play game
- Else

## How to deploy

- Put the files into a web server
- Change the configuration variables in `./config.php`
- Set a webhook to `./main.php`
