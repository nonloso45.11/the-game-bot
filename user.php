<?php

class User{
    
    public int $user_id;

    private PDO $db;

    private string $name; //50
    private string $language; //5

    private int $health; //-127 - 127 / 255 
    private int $happyness;

    private int $hunger; 
    private int $thirst;

    private float $money; //float
    private float $debt;
    private float $interest_rate = INTEREST_RATE;

    private int $inventory_space = 20;
    private array $inventory = array();

    private int $last_seen_online;

    public int $last_story;

    function __construct(int $user_id, PDO &$db){
        $this->db = $db;
        $this->user_id = $user_id;   
        
    }

    public function checkUserExist(){
        try{
            $query = $this->db->prepare("SELECT * FROM user WHERE user_id = :chid");
            $query->bindParam(':chid', $this->user_id);
            $query->execute();
        }
        catch(PDOException $e){
            return $e->getMessage();
            
        }
        
        $result = $query->fetchAll();

        if($result != []){
            foreach($result as $row){
               
                $this->name = isset($row['name']) ? $row['name'] : "";
                $this->language = isset($row['language']) ? $row['language'] : "en";
                $this->health = $row['health'];
                $this->happyness = $row['happyness'];
                $this->hunger = $row['hunger'];
                $this->thirst = $row['thirst'];
                $this->money = $row['money'];
                $this->debt = $row['debt'];
                $this->last_seen_online = isset($row['last_seen_online']) ? $row['last_seen_online'] : date("U");
                $this->last_story = $row['last_story'];

            }
            return "user exist";

        }
        else{
            return $this->createNewUser();
        }
    }

    private function createNewUser(){
        $now = date("U");
        try{
            $query = $this->db->prepare("INSERT INTO user(user_id, last_seen_online) VALUES(:chid, :lstsn)");
            $query->bindParam(':chid', $this->user_id);
            $query->bindParam(':lstsn', $now);

            $query->execute();
        }
        catch(PDOException $e){
            return $e->getMessage();
        }

        return "User Created";
    }


    public function setName(string $name){
        $name = preg_replace("/[a-zA-Z ]/", "", $name);

        if($name == ""){
            return "error";
        }

        $this->name = $name;

        try{
            $query = $this->db->prepare("UPDATE user SET name = :nme WHERE user_id = :chid");
            $query->bindParam(':nme', $this->name);
            $query->bindParam(':chid', $this->user_id);
            $query->execute();
        }
        catch(PDOException $e){
            return $e->getMessage();
        }
        
    }

    public function getName(){
        return $this->name;
    }

    public function getLanguage(){
        return $this->language;
    }

    public function changeHealth(int $amount){
        $this->health += $amount;

        if($this->health > 100){    $this->health = 100;    }
        if($this->health < 0){      $this->health = 0;      }

        try{
            $query = $this->db->prepare("UPDATE user SET health = :ht WHERE user_id = :chid");
            $query->bindParam(':ht', $this->health);
            $query->bindParam(':chid', $this->user_id);
            $query->execute();
        }
        catch(PDOException $e){
            return $e->getMessage();
        }

        return $this->health;
    }

    public function getHealth(){
        return $this->health;
    }

    public function changeHappyness(int $amount){
        $this->happyness += $amount;

        if($this->happyness > 100){ $this->happyness = 100; }
        if($this->happyness < 0){   $this->happyness = 0;   }

        try{
            $query = $this->db->prepare("UPDATE user SET happyness = :hp WHERE user_id = :chid");
            $query->bindParam(':hp', $this->happyness);
            $query->bindParam(':chid', $this->user_id);
            $query->execute();
        }
        catch(PDOException $e){
            return $e->getMessage();
        }

        return $this->happyness;
    }

    public function getHappyness(){
        return $this->happyness;
    }

    public function changeHunger(int $amount){
        $this->hunger += $amount;

        if($this->hunger > 100){    $this->hunger = 100;    }
        if($this->hunger < 0){      $this->hunger = 0;      }

        try{
            $query = $this->db->prepare("UPDATE user SET hunger = :hg WHERE user_id = :chid");
            $query->bindParam(':hg', $this->hunger);
            $query->bindParam(':chid', $this->user_id);
            $query->execute();
        }
        catch(PDOException $e){
            return $e->getMessage();
        }

        return $this->hunger;
    }

    public function getHunger(){
        return $this->hunger;
    }

    public function changeThirst(int $amount){
        $this->thirst += $amount;

        if($this->thirst > 100){    $this->thirst = 100;    }
        if($this->thirst < 0){      $this->thirst = 0;      }

        try{
            $query = $this->db->prepare("UPDATE user SET thirst = :th WHERE user_id = :chid");
            $query->bindParam(':th', $this->thirst);
            $query->bindParam(':chid', $this->user_id);
            $query->execute();
        }
        catch(PDOException $e){
            return $e->getMessage();
        }

        return $this->thirst;
    }

    public function getThirst(){
        return $this->thirst;
    }

    public function changeMoney(int $amount){
        $this->money += $amount;

        if($this->money < 0){
            $this->debt += abs($this->money) + (abs($this->money) * $this->interest_rate);
            $this->money = 0;
        }

        try{
            $query = $this->db->prepare("UPDATE user SET money = :mn WHERE user_id = :chid");
            $query->bindParam(':mn', $this->money);
            $query->bindParam(':chid', $this->user_id);
            $query->execute();
        }
        catch(PDOException $e){
            return $e->getMessage();
        }

        return $this->money;
    }

    public function getMoney(){
        return $this->money;
    }

    public function changeDebt(int $amount){
        $this->debt += $amount;
        if($this->debt < 0){
            $this->money += abs($this->debt);
            $this->debt = 0;
        }

        try{
            $query = $this->db->prepare("UPDATE user SET debt = :dbt WHERE user_id = :chid");
            $query->bindParam(':dbt', $this->debt);
            $query->bindParam(':chid', $this->user_id);
            $query->execute();
        }
        catch(PDOException $e){
            return $e->getMessage();
        }

        return $this->debt;
        
    }

    public function getDebt(){
        return $this->debt;
    }

    public function addToInventory($item, $number = 1){
        $itm = new $item();
        while($number > 0){

            if(sizeof($this->inventory) > $this->inventory_space){
                return "Full";
            }
            $this->inventory[] = $itm;
            $number--;
        }

        return $this->inventory;
    }

    public function removeFromInventory($item, $number = 1){
        $itm = new $item();

        while($number > 0){
            $n = 0;
            foreach($this->inventory as $i){
                // $j = $i[0];
                if($i == $itm){    
                    unset($this->inventory[$n]);
                    // return "trovato simile";
                    continue;
                }
                $n++; 
                
            }
            $number--;
        }
        return $this->inventory;
    }


    public function getInventory(){
        return $this->inventory;
    }

    public function getInventoryObject($item){
        $itm = new $item();

        foreach($this->inventory as $i){
            if($i == $itm){    
                return $i;
            }
        }

        return "object not found";
    }
        
    


    public function Drink($obj){
        if(!$obj->isDrinkable()){
            return "you can not drink this";
        }
        return "you can drink this";
      
    }

    public function Eat(Objectss $obj){
        if(!$obj->isEatable()){
            return "you can not eat this";
        }
        return "you can eat this";
      
    }

    private function drawStatusLine($what, $max){
        $line = "";
        $filled = round(($what * 10) / $max);
        $c = 0;
        for($i = 0; $i < $filled; $i++){
            $c++;
            $line .= "▰";
        }
        while($c < 10){
            $line .= "▱";
            $c++;
        }

        return $line;
    }

    public function getProfile(){
        $replace_vals = [
            $this->name,
            $this->health,
            $this->drawStatusLine($this->health, 100),
            $this->happyness,
            $this->drawStatusLine($this->happyness, 100),
            $this->hunger,
            $this->drawStatusLine($this->hunger, 100),
            $this->thirst,
            $this->drawStatusLine($this->thirst, 100),
            $this->money,
            $this->drawStatusLine($this->money, 100000000),
            $this->debt,
            $this->drawStatusLine($this->debt, 100000),
        ];

        return getTxt("user_profile", $replace_vals, $this->language); 
  
           
       
    }

}

?>
