<?php




$weakPotion = new HealthPotionWeak();
$pasta = new FoodPasta();
$itm = new HealthPotionWeak();

$user->addToInventory("HealthPotionWeak");
$user->setName("Carlo");
$user->changeHealth(-12);
$user->changeHappyness(-7);
$user->changeHunger(14);
$user->changeThirst(7);
$user->changeMoney(5000000);
$user->changeDebt(10000);

$bot->callApi("sendMessage", [  
    'chat_id' => ADMIN,
    'text' => $user->getProfile(),
    'parse_mode' => 'html'
]);

exit;

?>
